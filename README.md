# **Beerlog** #

A simple brewing logger written in Ruby to keep track of a persons home brews. A quick preview of the
application can be seen **[HERE](http://enigmatic-garden-23461.herokuapp.com/#/home)**

### How do set up on heroku ###

* Clone project
* Run "heroku create"
* Run "git push heroku master"
* To take advantage of Google captcha, generate your own CAPTCHA_KEY and add it to heroku's config variables, also
modify register_controller and add the public key to 'sitekey'.
* Start logging!

### Some features ###

* Authentication
* Recipe sharing
* Rating, and much more!

### LICENSE ###
MIT License

Copyright (c) 2016, Jaagup Viil

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.