class Recipe < ActiveRecord::Base
    has_one :ingredient
    has_one :stat
    attr_accessor :user, :rating
    
    def self.permitted_params
        [:name, :style, :batch_size, :notes]
    end
    
    
    def attributes
      super.merge('user' => self.user, 'rating' => self.rating)
    end
    
    def populateUserAndRating
        self.rating = 5
        beer = Beer.where(:recipe_id => self.id).first
        if (beer != nil)
            user = User.find_by_id(beer.user_id)
            if (user != nil)
                self.user = user.email 
            end
            if (beer.rating != nil)
                self.rating = beer.rating
            else
                self.rating = 0
            end
        end
    end
    
end
