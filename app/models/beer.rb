class Beer < ActiveRecord::Base
    
    #attr_accessor :name, :brewed, :bottled, :rating, :notes, :recipe_id, :stat_id
    
    has_one :stat
    has_one :recipe
    belongs_to :user
    
    def self.permitted_params
        [:beer, :name, :brewed, :bottled, :finished, :notes, :rating]
    end
    
end
