class User < ActiveRecord::Base
    has_secure_password

    validates :email, :password_digest, presence: true
    validates :email, uniqueness: { case_sensitive: false }
    
    validates :password, length: { minimum: 4 }
    
    has_many :beers, dependent: :destroy
end
