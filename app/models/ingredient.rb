class Ingredient < ActiveRecord::Base
    
    #This does not seem to work, only allows "other" parameter
    def self.permitted_params
        [:yeast, :hops, :other, :fermentables]
    end
    
end
