class Stat < ActiveRecord::Base
    
    def self.permitted_params
        [:OG, :FG, :IBU, :SRM]
    end
    
end

