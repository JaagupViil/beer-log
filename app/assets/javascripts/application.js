// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require angular/angular
//= require angular-busy/dist/angular-busy
//= require angular-route/angular-route
//= require angular-resource/angular-resource
//= require angular-rails-templates
//= require bootstrap-sass/assets/javascripts/bootstrap-sprockets
//= require moment/moment
//= require angular-bootstrap-datetimepicker/src/js/datetimepicker
//= require angular-bootstrap-colorpicker/js/bootstrap-colorpicker-module
//= require angular-wysiwyg/dist/angular-wysiwyg
//= require angular-utils-pagination/dirPagination
//= require toastr/toastr
//= require angular-google-chart/ng-google-chart.min
//= require pdfjs-dist/build/pdf
//= require pdfjs-dist/build/pdf.worker
//= require_tree .
