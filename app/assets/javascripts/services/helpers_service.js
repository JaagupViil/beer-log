'use strict';

var app = angular.module('beer');

app.factory('BeerHelper', ['$rootScope', '$http', '$location', function ($rootScope, $http, $location) {
    
    //Toast appearance time period
    toastr.options.timeOut = 2500;
    
    var helperFunctions = {
        
        pingServer : function() {
            $http.get('/staying/alive/ping').success(function (response) {}); 
        },
        
        calculateABV : function (OG,FG) {
            var ABV = (76.08 * (OG-FG) / (1.775-OG)) * (FG / 0.794);
            return Math.round(ABV * 100) / 100;
        },
        
        calculateBottleTime : function (bottled,finished) {
            if (finished) {
                return ((moment(finished).preciseDiff(moment(bottled))));
            } else {
                var now = moment().format('YYYY-MM-DD')
                if (bottled>now) {
                    return "Bottling date is in the future!";
                } else {
                    return ((moment(now)).preciseDiff(moment(bottled)));
                }
            }
        },


        
        addChoice : function (ingredient,type,scope) {
            if (!ingredient) {
                helperFunctions._addChoiceDependingOnType(type, '', '', scope);
            } 
            else {
                ingredient.push({name:'',amount:''});
            }
        },

        _addChoiceDependingOnType : function (type, name, amount, scope) {
            switch(type) {
                case INGREDIENT_TYPE.FERMENTABLES:
                    scope.ingredients.fermentables.push({name:name,amount:amount});
                    break;
                case INGREDIENT_TYPE.HOPS:
                    scope.ingredients.hops.push({name:name,amount:amount});
                    break;
                case INGREDIENT_TYPE.YEAST:
                    scope.ingredients.yeast.push({name:name});
                    break;
                case INGREDIENT_TYPE.OTHER:
                    scope.ingredients.other.push({name:name,amount:amount});
                    break;
            } 
        },
        
        removeChoice : function(ingredients,ingredient) {
            var i = ingredients.indexOf(ingredient);
            ingredients.splice(i,1);
        },
        
        validateDates : function(start,end) {
            //Moment - added js plugin time library
            var startDate = moment(start).isValid()
            var endDate = moment(end).isValid()
            
            if (startDate && endDate){
                if(moment(start) < moment(end)) {
                    return true;
                } else {
                    toastr.warning("Bottling date has to be before finishing date!");
                    return false;
                }
            } else {
                return true;
            }
        },
        
        validateOgFG : function(OG, FG) {
            var regex = /^[0-9]\.[0-9]+/;
            
            if (helperFunctions.notNullAndEmpty(OG)) {
                var invalidGravity = "";
    
                if (!OG.match(regex)) {
                    invalidGravity += "OG";
                }
                if (helperFunctions.notNullAndEmpty(FG) && !FG.match(regex)) {
                    invalidGravity == "" 
                        ? invalidGravity += "FG"
                        : invalidGravity += "/FG";
                }
                
                if (invalidGravity != "") {
                    toastr.warning(invalidGravity + " has to be a number with a decimal point");
                    return false;
                }
            }

            return true;
        },
        
        notNullAndEmpty : function(object) {
            return object != null && object != "";
        },
        
        setStarRating : function(id,scope) {
            var rating_value = id.siblings('input.rating-value').val();
            if (rating_value === "beer.rating") {
                rating_value = scope.beer.rating;
            } else if (rating_value === "recipe.rating"){
                rating_value = scope.recipe.rating;
            }
            return id.each(function() {
                if (rating_value >= parseInt($(this).data('rating'))) {
                    return $(this).removeClass('fa-star-o').addClass('fa-star');
                } else {
                    return $(this).removeClass('fa-star').addClass('fa-star-o');
                }
            });
        },
        
        starRatingListener : function(id,scope) {
            //Sets the default star rating from beer/recipe.rating
            helperFunctions.setStarRating(id,scope);
            //Update rating on click
            id.on('click', function() {
                id.siblings('input.rating-value').val($(this).data('rating'));
                var rating = (parseInt(id.siblings('input.rating-value').val()));
                if (id.selector.includes("beer")) {
                    scope.beer.rating = rating;
                } else {
                    scope.recipe.rating = rating;
                }
                return helperFunctions.setStarRating(id,scope);              
            });
        },
        
        setSRMBoxWidth :  function(scope,SRM) {
            var total_width = $('#SRM').width();
            if (total_width%2!=0) {
                total_width+=1
            }
            var srm_color = helperFunctions.SRM[SRM]
            var box_width = (total_width/(SRM+(40-SRM)));
            var tip_margin = (SRM-20)*box_width+(total_width/4);
            var color = "white";
            if (SRM<20) {
                color = "black";
            }
            if (SRM==40-1 && !scope.srm_last_backround) {
                scope.srm_last_background = { "background-color" : srm_color }
            }
            scope.srm_width =  {
                "width":box_width+"px"
            };
            scope.tip_margin = {
                "background-color" : srm_color,
                "border-top": "solid "+srm_color+" 10px",
                "margin-left" : tip_margin,
                "width": total_width/2 + "px",
                "color" : color
            }
        }, 

        addDateToNotes : function(prevNotes, newNotes) {
            if(newNotes != undefined && newNotes != null && newNotes != "") {
                var notesWereChanged = helperFunctions.hashCode(prevNotes) != helperFunctions.hashCode(newNotes);
                if (notesWereChanged) {
                    var noteDate = "Note added, (" + moment().format('YYYY, MMM D') + ")"
                    var notesDoNotContainTodaysDate = !newNotes.includes(noteDate);
                    
                    if (notesDoNotContainTodaysDate) {
                        if(newNotes.slice(-8).includes('<br>')) {
                            newNotes = newNotes + '<i>' + noteDate + '</i>';
                        } else {
                            newNotes = newNotes + '<br><i>' + noteDate + '</i>';
                        }    
                    }
                    
                }
            }
            return newNotes;
        },

        calculateSRM : function(fermentables, volume) {
            console.log("Calculating SRM from fermentables");
            //MCU = WLO/VOL
            //WLO: (Weight of grain in kg) x (Color of grain in degree lovibond x 2,205)
            //VOL: (Volume in L x 0,264)

            var MAX_SRM_ALLOWED = 40;
            var WLO = 0;
            var VOL = helperFunctions.volumeToLiters(volume) * 0.264;

            var len = fermentables.length;
            for (var i = 0; i < len; i++) {
                var name = fermentables[i].name;
                var weight = helperFunctions.amountToKg(fermentables[i].amount);

                var match = name.match(helperFunctions.getColorRegex())
                if (match != null) {
                    var color = match[0];
                    var colorValue = match[0].replace(/(EBC|SRM|<|\s)/g, "");

                    //if we have color range, take the average of min max
                    if (color.includes("-")) {
                        var values = color.split("-");
                        colorValue = (parseInt(values[0]) + parseInt(values[1])) / 2;
                    }

                    var lovibond;
                    if (color.includes("SRM")) {
                        lovibond = helperFunctions.SRMToLovibond(colorValue);
                    } else if (color.includes("EBC")) {
                        lovibond = helperFunctions.EBCToLovibond(colorValue);
                    }

                    WLO += (lovibond * weight * 2.205);
                }
            }

            var SRM = Math.min(MAX_SRM_ALLOWED, Math.round(WLO / VOL));
            console.log("SRM: ", SRM);
            return SRM - 1;
        },

        EBCToLovibond : function(ebc) {
            return helperFunctions.SRMToLovibond(helperFunctions.EBCToSRM(ebc));
        },

        SRMToLovibond : function(srm) {
            return (srm + 0.76) / 1.3546;
        },

        EBCToSRM : function(ebc) {
            return ebc * 0.508;
        },

        SRMToEBC : function(srm) {
            return srm * 1.97;
        },

        amountToKg : function(amount) {
            if (amount === undefined || amount === "") {
                return 0;
            }

            var kg = amount.toLowerCase().replace(/(kg|g|\s)/g, "");
            if (amount.toLowerCase().includes("kg")) {
                return parseFloat(kg);
            } else if (amount.toLowerCase().includes("g")) {
                return parseFloat(kg) / 1000;
            } else {
                //default to kg
                return parseFloat(kg);
            }
        },

        volumeToLiters : function(volume) {
            if (volume === undefined || volume === "") {
                return 0;
            }

            var liters = volume.toLowerCase().replace(/(ml|l|\s)/g, "");
            if (volume.toLowerCase().includes("ml")) {
                return parseInt(liters) / 1000;
            } else if (volume.toLowerCase().includes("l")) {
                return parseInt(liters);
            } else {
                //default to liters
                return parseInt(liters);
            }
        },


        readIngredientsFromPdf : function (pdf, scope){
            new PdfParser(pdf).parseIngredients().then(function (ingredients) {

                if (ingredients == null || ingredients.length == 0) {
                    return;
                }

                scope.prepareAddNewRecipe();

                for (var i = 0; i < ingredients.length; i++) {
                    var ingredient = ingredients[i];

                    var name = ingredient.name;
                    var amount = ingredient.amount;
                    var type = ingredient.type;

                    helperFunctions._addChoiceDependingOnType(type, name, amount, scope);
                }

                //refresh the ingredients added
                scope.$apply();
            });
        },

        
        hashCode : function(s) {
            if (s === null || s === undefined) {
                return "";
            }
            return s.split("").reduce(function(a,b){a=((a<<5)-a)+b.charCodeAt(0);return a&a},0);              
        },

        /**
            Regex to determine if a fermentable field contains also the grains color
        */  
        getColorRegex : function() { return /<?\d+(-\d+)?\s?(EBC|SRM)/ },
                
        notesMenu : [
            ['bold', 'italic', 'underline', 'strikethrough', 'subscript', 'superscript'],
            ['format-block'],
            ['font'],
            ['font-size'],
            ['font-color', 'hilite-color'],
            ['remove-format'],
            ['ordered-list', 'unordered-list'],
            ['left-justify', 'center-justify', 'right-justify'],
            ['code', 'quote', 'paragraph'],
            ['image']
        ],
        
        SRM : ["#f7e1a1","#f0c566","#e9ad3f","#e19726","#d98416","#d1730c",
               "#c86505","#c05801","#b74d00","#af4300","#a73b00","#9f3400",
               "#972d00","#8f2800","#882300","#811f00","#7b1b00","#741800",
               "#6e1500","#681200","#631000","#5e0e00","#590c00","#540b00",
               "#500900","#4c0800","#480700","#440600","#410500","#3d0500",
               "#3a0400","#370400","#340300","#320300","#2f0200","#2d0200",
               "#2a0200","#280100","#260100","#160000"]

    }
    
    
    return helperFunctions;
    
}]);