//'non-ng' functions for /beers page

viewBeer = function(element, event) {
    window.location.href = `#/beers/${element.id}`;
}

editBeer = function(element, event) {
	var clickedMiddleButton = (event.which == 2);

	if (clickedMiddleButton) {
    	window.location.href = `#/beers/${element.id}/edit_beer`;
	}
}