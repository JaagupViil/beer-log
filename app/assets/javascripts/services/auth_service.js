'use strict';

var app = angular.module('beer');

app.service('AuthService', ['$http', function ($http) {
    var counter = 0;
    var authFunctions = {
        
        checkTokenStatus : function() {
            var token = localStorage.getItem('auth_token');
            //console.log("Checking token authentication from API, token:",token);
            
            if (token) {
                token = angular.fromJson(token).token;
            }
            
            return $http.get('/auth/token_status?token=' + token);
        },
            
        signIn : function(user) {
            return $http.post('/auth/authenticate',user);
        },
        
        signOut : function() {
            localStorage.removeItem('auth_token');
            //Force reload
            window.location.href = "#/login";
            window.location.reload();
        },
        
        isLoggedIn : function() {
            return (localStorage.getItem('auth_token'));
        },
        
        getUserEmail : function() {
            if (this.isLoggedIn()) {
                var user_name = JSON.parse(localStorage.getItem('auth_token')).user.email;
                return user_name;
            }
        },
        
        userService : function(url,user) {
            return $http.post(url,user).success(function(response) {
                var msg = response.message;
                console.log(msg)
                if(msg.indexOf("Problem") > -1 || msg.indexOf("wrong") > -1) {
                    toastr.error(msg);
                } else {
                    if (response.email) {
                        var token = {user:{email:response.email},token:response.token}
                        localStorage.setItem('auth_token',JSON.stringify(token));
                    }
                    toastr.success(msg);
                }
            }).error(function (response) {
                var msg = response.message;
                grecaptcha.reset();
                for (var error in msg) {
                    toastr.error(error+" "+msg[error]);
                }
            });
        }
        
    };
    return authFunctions;
}]);