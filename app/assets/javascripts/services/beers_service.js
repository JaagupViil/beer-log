'use strict';

var app = angular.module('beer');

app.factory('BeerService', ['$rootScope', '$http', 'BeerHelper', '$location', function ($rootScope, $http, BeerHelper, $location) {

    return {
        getBeerData : function(id, scope) {
            return $http.get('/beers/?id='+id).success(function (response) {
                //console.log("Getting beer data, with id: "+id)
                var msg = response.message;
                if(msg=="This beer does not exist") {
                    toastr.error(msg);
                    $location.path("/");
                }
                else {
                    $rootScope.beer = msg.beer;
                    $rootScope.beer_notes = msg.beer.notes;
                    $rootScope.beer_stats = msg.beer_stats;
                    $rootScope.recipe = msg.recipe;
                    $rootScope.recipe_stats = msg.recipe_stats;
                    $rootScope.ingredients = msg.ingredients;
                    $rootScope.srmArray = BeerHelper.SRM.slice(0,msg.beer_stats.SRM+1);
                    $rootScope.beer.abv = BeerHelper.calculateABV(msg.beer_stats.OG,msg.beer_stats.FG);
                    $rootScope.beer.bottle_time = BeerHelper.calculateBottleTime(msg.beer.bottled,msg.beer.finished);
                    BeerHelper.setSRMBoxWidth(scope, msg.beer_stats.SRM);
                }
                
            });
        },
        
        getRecipeData : function(id, scope) {
            return $http.get('recipes/?id='+id).success(function (response) {
               // console.log("Getting recipe data, with id: "+id);
                var msg = response.message;
                if(msg=="This recipe does not exist!") {
                    toastr.error(msg);
                    $location.path("/");
                } else {
                    scope.recipe = msg.recipe;
                    scope.ingredients = msg.ingredients;
                    scope.recipe_stats = msg.stats;
                    scope.srmArray = BeerHelper.SRM.slice(0,scope.recipe_stats.SRM+1);
                    scope.recipe.abv = BeerHelper.calculateABV(msg.stats.OG,msg.stats.FG);;
                    BeerHelper.setSRMBoxWidth(scope,msg.stats.SRM);
                    BeerHelper.starRatingListener($('#star-rating-recipe .fa'),scope);
                }
            });
        },
        
        getRecipes : function(scope) {
            return $http.get('/recipes').success(function (response) {
               // console.log(response.message.recipe);
                scope.recipes = response.message.recipes;
            });
        },
        

        getUserBeers : function(scope) {
            return $http.get('/beers').success(function (response) {
                scope.beers = response.message.beers;
                var index = 1;
                scope.beers.reverse().forEach(function(beer) {
                    beer.nr = index;
                    index++;
                });
            });
        },
        
        
        updateRecord : function(url,record) {
            //console.log("updating record")
            $http.post(url,record).success(function (response) {
                var msg = response.message;
                if (msg.includes("Something went wrong")) {
                    toastr.error(msg)
                }
                else {
                    //When adding a new beer, redirect to the beers page
                    toastr.success(msg)
                    if (response.id) {
                        $location.path("/beers/"+response.id);
                    } else {
                        $location.path('beers/'+$rootScope.beer.id);
                    }
                }
            });
        },
        
        deleteRecord : function(id) {
            $http.get('/beers/delete?id='+id).success(function (response) {
                var msg = response.message;
                if(msg.includes("Something went wrong")) {
                    toastr.error(msg);
                } else {
                    toastr.success(msg);
                }
                
            });
        },
                
        resetData : function() {
            $rootScope.beer = null;
            $rootScope.beer_stats = null;
            $rootScope.recipe = null;
            $rootScope.recipe_stats = null;
            $rootScope.ingredients = null;
            $rootScope.srmArray = null;
        },
        
        getGeneralStatistics : function(scope) {
            return $http.get('/beers/statistics').success(function (response) {
                
                var msg = response.message;
                scope.total = msg.total;
                scope.styles_count = msg.styles_count;
                scope.drawPieChart();
            });
        }
        
        
    };
    
}]);