/**
	Parses 'Pruulikeskus' invoice pdf into set of ingredients
*/
class PdfParser extends IngredientsParser {

	get VALID_PDF_KEYS() {
		return [
			"www.pruulikeskus.ee",
			"Pruulikeskus OÜ"
		]
	}

	get TABLE_STARTING_HEADER() {
		return [
                "tootenimi/productname", 
                "kogus/qty", 
                "hind/price", 
                "soodushind/discountprice", 
                "soodustustk/discountpcs", 
                "kokku/total"
            ];
	};

	get TABLE_END() {
		return "vahesumma";
	}

	get AMOUNT_IDX_IN_HEADER() {
		return 1;
	}

	get YEAST_KEYWORDS() {
		return ["pärm", "yeast"];
	}

	get FERMENTABLE_KEYWORDS() {
		return [
				"malt", "EBC", "SRM", "oder", 
				"odra", "barley", "LME", "DME", "Maris otter",
				"kuivekstrakt"
		];
	}

	get HOP_KEYWORDS() {
		return ["hop", "graanul", "humal"];
	}

    constructor(pdfUrl) {
        super();
        
        pdfjsLib.workerSrc = '/pdfjs-dist/build/pdf.worker.js';

        this.pdfUrl = pdfUrl;
    }

    parseIngredients() {
    	var parser = this;

    	var ingredients = parser._parseIngredientNamesAndAmounts().then(function(ingredients) {
    		if (ingredients === undefined) {
				return [];
    		}

    		return parser._extractIngredientAmounts(
				parser._identiftyIngredientTypes(ingredients[0])
			);
    	});

    	return ingredients;
    }

    _parseIngredientNamesAndAmounts() {
		var pdf = pdfjsLib.getDocument(this.pdfUrl);
        var parser = this;

        return pdf.promise.then(function(pdf) {
         	var countPromises = [];
         	var page = pdf.getPage(1);

            countPromises.push(page.then(function(page) {

                var textContent = page.getTextContent();

                return textContent.then(function(text){

                	var valid = parser._isValidPdf(text);

                	if (!valid) {
            			toastr.error("Not a valid Pruulikeskus invoice");
                		return [];
                	}

                    return parser._doParseIngredient(
                    	parser._getIngredientsStartIndex(text), 
                    	text
                	);

                });

            }));
      

         	return Promise.all(countPromises).then(function (ingredients) {
         		return ingredients;
         	});

        }).catch(function(pdf) {
        	toastr.error("Invalid pdf");
        });
    }

    _isValidPdf(text) {
    	for (var i = 0; i < text.items.length; i++) {
    		var str = text.items[i].str;
    		if (this._strContainsAnyOf(str, this.VALID_PDF_KEYS)) {
    			return true;
    		};
		}

		return false;
    }

    _getIngredientsStartIndex(text) {
    	var currentTableHeader = "";
    	var tableHeader = this.TABLE_STARTING_HEADER;

        for (var i = 0; i < text.items.length; i++) {
    		var iteratingOverIngredients = (tableHeader.length == 0);

    		if (iteratingOverIngredients) {
    			return i;
    		} else {
	            var convertedStr = text.items[i].str.toLowerCase().replace(/\s/g, "");
	            var strExistsInHeader = tableHeader.filter(function(hdr){ return hdr.includes(convertedStr) }).length != 0;
	            if (strExistsInHeader) {
	                currentTableHeader += convertedStr;
	            }

	            var canRemoveTableHeader = tableHeader.filter(function(hdr){ return hdr === currentTableHeader }).length != 0;
	            if (canRemoveTableHeader) {
	                tableHeader.shift();
	                currentTableHeader = "";
	            }
    		}

    	}

    	return -1;
    }

    _doParseIngredient(start, text) {
        var ingredients = new Array();
        var currentIngredientStr = "";

        var newRow = true;
        var countSinceRowChange = 0;

        var leftMostX = -1;

        for (var i = start; i < text.items.length; i++) {
        	var pdfStr = text.items[i].str;

    		var reachedEndOfTable = pdfStr.toLowerCase().replace(/\s/g, "").includes(this.TABLE_END) == true;
            if (reachedEndOfTable) {
                break;
            }

	        var x = text.items[i].transform[4];
	        var y = text.items[i].transform[5];

	        if (leftMostX == -1) {
	        	leftMostX = x;
	        }

			currentIngredientStr += (currentIngredientStr == "") ? pdfStr : " " + pdfStr;

	        if (i + 1 < text.items.length) {
	            var nextX = text.items[i + 1].transform[4];
	            var nextY = text.items[i + 1].transform[5];

            	var columnChanged = (x != nextX);
	            if (columnChanged) {

            		var rowChanged = (x == leftMostX);
	                if (rowChanged) {
	                	newRow = true;
	                	countSinceRowChange = 0;
	                }

	                if (newRow) {
	                    var product = {}
	                    product.name = currentIngredientStr;
	                    product.amount = 0;
	                    ingredients.push(product);

	                    newRow = false;
	                } else {
	                	countSinceRowChange++;
	                }

                	var columnIsAmountColumn = (countSinceRowChange == this.AMOUNT_IDX_IN_HEADER)
	                if (columnIsAmountColumn) {
	                    ingredients[ingredients.length - 1].amount = currentIngredientStr;
	                }
	                
	                currentIngredientStr = "";
	            }

	        }

    	}

    	return ingredients;
    }

    _extractIngredientAmounts(ingredients) {
    	for (var i = 0; i < ingredients.length; i++) {
    		var ingredient = ingredients[i];
    		
    		var name = ingredient.name;
    		var amount = ingredient.amount;
    		var type = ingredient.type;

    		if (type == INGREDIENT_TYPE.YEAST) {
    			continue;
    		}

    		var amountFromName = name.match(/\d+((\.|,)\d+)?(g|kg)/);
    		if (amountFromName != null) {
    			var newAmount = amountFromName[0];

    			//remove the amount info from name
    			ingredient.name = name.replace(newAmount, "");

    			newAmount = newAmount.replace(",", ".");
    			
    			//multiply the amount info gotten from the name with the actual amount and replace it
    			if (newAmount.includes("kg")) {
					ingredient.amount = (parseFloat(newAmount) * amount) + "kg";
    			} else if (newAmount.includes("g")) {
					ingredient.amount = (parseFloat(newAmount) * amount) + "g";
    			}

			} else {
				if (type == INGREDIENT_TYPE.FERMENTABLES) {
					//default to kg
					ingredient.amount = amount + "kg";
				}
			}
    	
    	}

    	return ingredients;
    }

    _identiftyIngredientTypes(ingredients) {
    	
    	for (var i = 0; i < ingredients.length; i++) {
    		var ingredient = ingredients[i];
    		var name = ingredient.name;
    		
    		if (this._strContainsAnyOf(name, this.YEAST_KEYWORDS)) {
				ingredient.type = INGREDIENT_TYPE.YEAST;
    		} else if (this._strContainsAnyOf(name, this.FERMENTABLE_KEYWORDS)) {
				ingredient.type = INGREDIENT_TYPE.FERMENTABLES;
    		} else if (this._strContainsAnyOf(name, this.HOP_KEYWORDS)) {
    			ingredient.type = INGREDIENT_TYPE.HOPS;
    		} else {
    			ingredient.type = INGREDIENT_TYPE.OTHER;
    		}

    	}

    	return ingredients;
    }

    _strContainsAnyOf(str, list) {
    	for (var i = 0; i < list.length; i++) {
    		if (str.toLowerCase().includes(list[i].toLowerCase())) {
    			return true;
    		}
    	}

    	return false;
    }
	
}