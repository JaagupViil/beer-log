class IngredientsParser {

    constructor() {
        if (this.constructor === IngredientsParser) {
            throw new TypeError('Abstract class "IngredientsParser" cannot be instantiated directly.'); 
        }

        if (this.parseIngredients === undefined) {
            throw new TypeError('Classes extending the PdfParser abstract class should override parseIngredients');
        }
    }
 
    parseIngredients() {

    }

}