var INGREDIENT_TYPE = {
  FERMENTABLES : 'fermentables',
  HOPS : 'hops',
  YEAST : 'yeast',
  OTHER : 'other'
};
