'use strict'

var app = angular.module('beer');
app.controller('BeersCtrl',BeersCtrl)

function BeersCtrl($scope, $routeParams, $location, BeerService, BeerHelper, $window) {
    //Pagination
    $scope.currentPage = 1;
    $scope.pageSize = 10;
    $scope.predicate = 'brewed';
    $scope.reverse = true;
    
    $scope.noMenu = []; //Empty menu = no menu for the textarea
    BeerService.resetData(); //Reset current beer info to zero if visiting beers page
    
    var id = parseInt($routeParams.id);
    if ($routeParams.id===undefined) {
        $scope.loadBeersData = BeerService.getUserBeers($scope);
    } else if (id) { 
        $scope.loadBeerData = BeerService.getBeerData(id,$scope).success(function (callback) {
           BeerHelper.setStarRating($('#star-rating-beer .fa'),$scope); 
        });
    } else {
        $location.path("/");
    }

    $scope.order = function(predicate) {
        $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
        $scope.predicate = predicate;
    };
    
    $scope.deleteBeer = function() {
        BeerService.deleteRecord(id);
        //Remove modal
        $("#confirmDelete").modal('hide');
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();
        $location.path("/beers");
    };
    angular.element($window).bind('resize', function() {
        if($scope.beer_stats) {
            BeerHelper.setSRMBoxWidth($scope, $scope.beer_stats.SRM);
            $scope.$digest();
        }
    });
    
}

BeersCtrl.$inject = ['$scope','$routeParams','$location', 'BeerService', 'BeerHelper', '$window'];