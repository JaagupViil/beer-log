'use strict';

var app = angular.module('beer');
app.controller('NavBarCtrl', NavBarCtrl);

function NavBarCtrl($scope, $location, AuthService, $rootScope) {
    $rootScope.$on("$routeChangeStart", function(event, next, current) {
        if ($location.path() == '/home' || $location.path() == '/recipes') {
            var valid_token = false;
            AuthService.checkTokenStatus().success(function (response) {
                if (response.error == null) {
                    valid_token = true;
                } else {
                    localStorage.removeItem('auth_token');
                }
            })
            $scope.isLoggedIn = function() {
                return valid_token;
            };
        }
        else {
            $scope.isLoggedIn = AuthService.isLoggedIn;
        }

    })

    $scope.logOut = function() {
        AuthService.signOut();
        $location.path('/login');
    };

}

NavBarCtrl.$inject = ['$scope', '$location', 'AuthService', '$rootScope'];