'use strict';

var app = angular.module('beer');
app.controller('ProfileCtrl',ProfileCtrl);

function ProfileCtrl($scope,$rootScope,$http,$location, AuthService, BeerService) {
    // console.log("Profile controller");
    
    $scope.email = AuthService.getUserEmail();
    if($location.path()!='/profile/edit') {
        $scope.loadGeneralStatistics = BeerService.getGeneralStatistics($scope);
    }
    
    $scope.editUser = function() {
        var pw = $scope.password;
        var pw_r = $scope.password_repeat;
        var email = $scope.email;
        if (pw != pw_r) {
            toastr.error("Passwords do not match!");
        } else {
            var new_user = {email:email, password:pw};
            AuthService.userService('/user/update',new_user).success(function(callback) {
                $location.path('/profile');
            });
        }
    };
    
    $scope.deleteUser = function() {
        $http.get('/user/delete').success(function(callback) {
            AuthService.signOut();
        });
    };
    
    $scope.drawPieChart = function() {
        $scope.chartObject = {};
        $scope.chartObject.type = "PieChart";
        $scope.chartObject.data = [['Style','Count']];
        for (var style in $scope.styles_count) {
            $scope.chartObject.data.push([''+style,$scope.styles_count[style]])
        }
    }
    
    $(document).ready(function() {
        $('#save').click(function(event){
            $('#submit').click()
        });
    });

}
ProfileCtrl.$inject = ['$scope','$rootScope','$http', '$location', 'AuthService', 'BeerService'];