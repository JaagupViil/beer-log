'use strict';

var app = angular.module('beer');
app.controller('BeerEditCtrl',BeerEditCtrl);

function BeerEditCtrl($scope,$rootScope, $http, $routeParams, $location, BeerHelper, BeerService) {

    $scope.SRM = BeerHelper.SRM;
    $scope.addNewChoice = BeerHelper.addChoice;
    $scope.removeChoice = BeerHelper.removeChoice;
    $scope.menu = BeerHelper.notesMenu;
    $scope.currentDate = new Date();
    
    $scope.lastPingTime = new Date();
    const PING_INTERVAL_MS = 10000;
    
    var id = parseInt($routeParams.id);
  
    var setRatingOnLoad = function() {
        BeerHelper.starRatingListener($('#star-rating-beer .fa'),$scope);
        BeerHelper.starRatingListener($('#star-rating-recipe .fa'),$scope);
    };
  
    if (id && !$rootScope.beer) {
      BeerService.getBeerData(id,$scope).success(function (callback) {
        setRatingOnLoad();
      });
    } else {
      setRatingOnLoad();
    }
  
    $scope.submitBeerEdit = function() {
      var brewed = document.getElementById('brewed').value;
      var bottled = document.getElementById('bottled').value;
      var finished = document.getElementById('finished').value;
      if (!bottled) {
        bottled = $rootScope.beer.bottled;
      }
      if (!brewed) {
        brewed = $rootScope.beer.brewed;
      }
      if (!finished) {
        finished = $rootScope.beer.finished;
      }

      var edit_beer_values = 
      {id:$rootScope.beer.id,beer:{name:$scope.beer.name,brewed:brewed,bottled:bottled,finished:finished,
      rating:$scope.beer.rating,notes:$scope.beer.notes}};
      
      if(BeerHelper.validateDates(bottled,finished)) {
        edit_beer_values.beer.notes = BeerHelper.addDateToNotes($rootScope.beer_notes, edit_beer_values.beer.notes);
        BeerService.updateRecord('/beers/update',edit_beer_values);
      }
      
    };
    
    /**
     Heroku likes to spin-down after some time, lets avoid this while writing notes.
    */
    $scope.pingServer = function() {
      var timeDiff = new Date().getTime() - $scope.lastPingTime.getTime();

      if (timeDiff >= PING_INTERVAL_MS) {
        BeerHelper.pingServer();
        $scope.lastPingTime = new Date();
      }
    };
    
    $scope.submitRecipeEdit = function() {
      var recipe = $rootScope.recipe;
      var stats = $rootScope.recipe_stats;
      var ingredients = $rootScope.ingredients;
      var edit_recipe_values = {
        recipe : {
          id:recipe.id, name:recipe.name, style:recipe.style, 
          batch_size:recipe.batch_size, notes:recipe.notes
        },
        ingredients: { 
          hops:ingredients.hops, fermentables:ingredients.fermentables,
          yeast:ingredients.yeast, other:ingredients.other
        },
        recipe_stats : {
          OG:stats.OG, FG:stats.FG,IBU:stats.IBU,SRM:stats.SRM
        }
      };

      if (BeerHelper.validateOgFG(stats.OG, stats.FG)) {
        BeerService.updateRecord('/recipes/update',edit_recipe_values);
      }
    };
    
    $scope.submitStatsEdit = function() {
      var stats = $rootScope.beer_stats;
      var edit_stats_values = {
        id: $scope.beer.id,
        stats : {
          OG:stats.OG, FG:stats.FG, IBU:stats.IBU, SRM:stats.SRM
        }
      };
      if (BeerHelper.validateOgFG(stats.OG, stats.FG)) {
        BeerService.updateRecord('/stats/update',edit_stats_values);
      }
    };

    //Use a fontawesome icon with text to submit the form
    $(document).ready(function() {
        $('#save').click(function(event){
            $('#submit').click()
        });
    });
    
    
    
};

BeerEditCtrl.$inject = ['$scope','$rootScope','$http','$routeParams','$location', 'BeerHelper', 'BeerService']