'use strict'

var app = angular.module('beer');
app.controller('BeerNewCtrl',BeerNewCtrl)

function BeerNewCtrl($scope,$rootScope, $http, $routeParams, $location, BeerHelper, BeerService) {
    
    BeerService.resetData();

    $scope.SRM = BeerHelper.SRM;
    $scope.beer = {};
    $scope.currentDate = new Date();
    
    $scope.beer_stats = {SRM:0};
    $scope.addNewChoice = BeerHelper.addChoice;
    $scope.removeChoice = BeerHelper.removeChoice;
    $scope.menu = BeerHelper.notesMenu;

    $scope.copyBeerStatsFromRecipe = true;
    $scope.canAutoCalcSRM = false;

    function clearDate(data) {
        $scope.data.date = 0;
    }
    
    $scope.prepareAddNewRecipe = function() {
        $scope.new_recipe = true;
        $scope.recipe = {};
        $scope.recipe.notes = ""
        $scope.ingredients = {};
        $scope.hops = {};
        $scope.yeast = {};
        $scope.other = {};
        $scope.recipe_stats = {SRM:0};
        BeerHelper.starRatingListener($('#star-rating-recipe .fa'),$scope);


        $scope.ingredients.fermentables = new Array();
        $scope.ingredients.hops = new Array();
        $scope.ingredients.yeast = new Array();
        $scope.ingredients.other = new Array();
    }
    
    $scope.selectFromRecipes = function() {
        // console.log("Getting recipes");
        $scope.currentPage = 1;
        $scope.pageSize = 7;
        $scope.predicate = 'rating';
        $scope.reverse = true;
        $scope.noMenu = []; 
        $scope.loadRecipes = BeerService.getRecipes($scope);
        $scope.order = function(predicate) {
            $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
            $scope.predicate = predicate;
        };
    };
    
    $scope.getRecipeData = function(id) {
        $scope.loadRecipe = BeerService.getRecipeData(id,$scope);
        $scope.new_recipe = true;
    };
    
    
    $scope.addNewBeer = function() {
        var brewed = document.getElementById('brewed').value;
        var bottled = document.getElementById('bottled').value;
        var finished = document.getElementById('finished').value;
        var beer_values = 
            {name:$scope.beer.name,brewed:brewed,bottled:bottled,finished:finished,
            rating:$scope.beer.rating,notes:$scope.beer.notes};
        
        var recipe = $scope.recipe;
        
        if(recipe == null) {
            toastr.warning("Please add a recipe!");
        } else {
            var recipe_stats = $scope.recipe_stats;
            var ingredients = $scope.ingredients;
            var recipe_values = {
                  id: recipe.id, name:recipe.name, style:recipe.style, 
                  batch_size:recipe.batch_size, notes:recipe.notes
            }
            
            var ingredient_values = { 
                  hops:ingredients.hops, fermentables:ingredients.fermentables,
                  yeast:ingredients.yeast, other:ingredients.other
            }
            var recipe_stats_values = {
                  OG:recipe_stats.OG, FG:recipe_stats.FG,IBU:recipe_stats.IBU,SRM:recipe_stats.SRM
            }

            var stats = $scope.beer_stats;
            var stats_values = {}

            if ($scope.copyBeerStatsFromRecipe) {
                var stats_values = {
                    OG:recipe_stats.OG, FG:recipe_stats.FG, IBU:recipe_stats.IBU, SRM:recipe_stats.SRM
                };
            } else {
                var stats_values = {
                    OG:stats.OG, FG:stats.FG, IBU:stats.IBU, SRM:stats.SRM
                };
            }

            
            var new_beer = {beer:beer_values, recipe:recipe_values, 
            ingredients:ingredient_values, recipe_stats:recipe_stats_values, beer_stats: stats_values}
            
            if(BeerHelper.validateDates(bottled,finished)) {
                new_beer.beer.notes = BeerHelper.addDateToNotes(new_beer.beer.notes);
                BeerService.updateRecord('/beers/create',new_beer)
            }
        }
        
      
    };

    $scope.loadFromPdf = function() {
        var input = document.getElementById('pdfInput');

        input.onchange = function(evt) {
            console.log("Loading..");

            var tgt = evt.target || window.event.srcElement;
            var files = tgt.files;

            if (FileReader && files && files.length) {
                var reader = new FileReader();

                reader.onload = function() {
                    BeerHelper.readIngredientsFromPdf(new Uint8Array(this.result), $scope);
                };

                reader.readAsArrayBuffer(files[0]);
            } else {
                toastr.warning("File reading not supported by the browser");
            }
        };

        input.click();
    }

    $scope.calculateSRM = function() {
        $scope.recipe_stats.SRM = BeerHelper.calculateSRM($scope.ingredients.fermentables, $scope.recipe.batch_size);
    };

    $scope.checkAutoCalculateSRMFlag = function() {
        var len = $scope.ingredients.fermentables.length;
        for (var i = 0; i < len; i++) {
            var name = $scope.ingredients.fermentables[i].name;
            var amount = $scope.ingredients.fermentables[i].amount;

            var batchIsDefined = $scope.recipe.batch_size != undefined && $scope.recipe.batch_size != "";
            var amountIsDefined = amount != undefined && amount != "";

            if (batchIsDefined && name.match(BeerHelper.getColorRegex()) != null && amountIsDefined) {
                $scope.canAutoCalcSRM = true;
                return;
            }
        }

        $scope.canAutoCalcSRM = false;
    };

    BeerHelper.starRatingListener($('#star-rating-beer .fa'),$scope);
    
    //Use a fontawesome icon with text to submit the form
    $(document).ready(function() {
        $('#save').click(function(event){
            $('#submit').click()
        });
    });
     
    
};

BeerNewCtrl.$inject = ['$scope','$rootScope','$http','$routeParams','$location', 'BeerHelper', 'BeerService']