'use strict';

var app = angular.module('beer');
app.controller('LoginCtrl',LoginCtrl);

function LoginCtrl($scope,$rootScope,$http,$location, AuthService) {
    var token = localStorage.getItem('auth_token');
    
    if(token) {
        AuthService.checkTokenStatus().then(function successCallback(response) {
            $location.path('/beers');
        },function errorCallback(response) {
            localStorage.removeItem('auth_token');
        });
    }

    $scope.submitSignIn = function () {
        if($scope.email && $scope.password) {
            var user = {email: $scope.email, password: $scope.password};
            AuthService.signIn(user).then(function successCallback(response) {
                // console.log("Signing in");
                localStorage.setItem('auth_token',JSON.stringify(response.data));
                $location.path("/beers");
            });
        }
    };
}

LoginCtrl.$inject = ['$scope','$rootScope','$http', '$location', 'AuthService'];