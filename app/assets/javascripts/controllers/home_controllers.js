'use strict';

var app = angular.module('beer');
app.controller('HomeCtrl',HomeCtrl);

function HomeCtrl($scope, $location, AuthService) {
    $scope.isLoggedIn = AuthService.isLoggedIn;
    $scope.logOut = function() {
        AuthService.signOut();
        $location.path('/login');
    };
    
}

HomeCtrl.$inject = ['$scope', '$location', 'AuthService'];