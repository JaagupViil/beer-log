'use strict';

var app = angular.module('beer');
app.controller('RegisterCtrl',RegisterCtrl);

function RegisterCtrl($scope,$rootScope,$http,$location, AuthService) {
    
    var captchaWidgetId = 
        grecaptcha.render( 'myCaptcha', {
          'sitekey' : '6LfalRcTAAAAANv7yuAm1Tt-pq0mi8vNvOjUBb5J',  
          'theme' : 'dark',  
        });
        
    $scope.registerUser = function() {
        var pw = $scope.password;
        var pw_r = $scope.password_repeat;
        var email = $scope.email;
        var response = grecaptcha.getResponse();
        if (pw != pw_r) {
            toastr.error("Passwords do not match!")
        } else if (!response) {
            toastr.error("Please fill captcha!")
        } else {
            var new_user = {email:email, password:pw, captcha_response:response};
            AuthService.userService('/auth/register',new_user).then(function successCallback(response) {
                if (response.status >= 200 && response.status <= 300) {
                    AuthService.signIn(new_user).then(function successCallback(response) {
                        localStorage.setItem('auth_token',JSON.stringify(response.data));
                        $location.path("/beers");
                    });
                }
            });
        }
    };

}
RegisterCtrl.$inject = ['$scope','$rootScope','$http', '$location', 'AuthService'];