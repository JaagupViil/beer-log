'use strict'

var app = angular.module('beer');
app.controller('RecipesCtrl',RecipesCtrl)

function RecipesCtrl($scope, $rootScope, $http, $routeParams, $location, BeerHelper, BeerService, $window) {
    //Pagination
    $scope.currentPage = 1;
    $scope.pageSize = 10;
    $scope.predicate = 'created_at';
    $scope.reverse = true;
    $scope.noMenu = []; //Empty menu = no menu for the textarea
    
    var id = parseInt($routeParams.id)
    if ($location.path()!='/home') {
        BeerService.resetData();
        if ($routeParams.id === undefined) {
            $scope.loadRecipes = BeerService.getRecipes($scope)
        } else if(id) {
            $scope.loadRecipe = BeerService.getRecipeData(id,$scope)
        } else {
            $location.path("/")
        }
    }
    $scope.searchRecipe = function(recipe_query) {
        $location.path("/recipes")
        $rootScope.q = recipe_query
    }
    
    $scope.order = function(predicate) {
        $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
        $scope.predicate = predicate;
    };
    
    angular.element($window).bind('resize', function() {
        if($scope.recipe_stats) {
            BeerHelper.setSRMBoxWidth($scope, $scope.recipe_stats.SRM);
            $scope.$apply();
        }
    });
    


    
};

RecipesCtrl.$inject = ['$scope','$rootScope','$http','$routeParams','$location', 'BeerHelper', 'BeerService', '$window']