var app = angular.module('beer',[
  'templates',
  'ngRoute',
  'ngResource',
  'cgBusy', //loading with $q
  'ui.bootstrap.datetimepicker', //datetime picker
  'wysiwyg.module', //richt text editor
  'angularUtils.directives.dirPagination', //pagination
  'googlechart'
])

//Detect route changes manually
app.run(['$rootScope','$location','AuthService', 'BeerService',function($rootScope,$location,AuthService, BeerService) {
        $rootScope.$on( "$routeChangeStart", function(event, next, current) {
            if (next.templateUrl != 'user/login.html' && $rootScope.login_errorMsg) {
                $rootScope.login_errorMsg = "";
            }
            //Clear the search variable
            if (next.templateUrl != 'recipes/recipes.html' && $rootScope.q) {
                $rootScope.q = undefined;
            }
            if (next.templateUrl === 'recipes/recipe.html' && $rootScope.recipe) {
                BeerService.resetData();
            }
        });
}]);

app.config(['$routeProvider', function($routeProvider){
    $routeProvider
        .when('/login', {
            templateUrl: "user/login.html",
            controller: 'LoginCtrl'
        })
        .when('/register', {
            templateUrl: "user/register.html",
            controller: 'RegisterCtrl'
        })
        .when('/profile', {
            templateUrl: "user/profile.html",
            controller: 'ProfileCtrl',
            resolve: {
                data : ['AuthService', function (AuthService) {
                    if (!AuthService.isLoggedIn()) {
                      return AuthService.checkTokenStatus()
                    }
                }] 
            },
        })
        .when('/profile/edit', {
            templateUrl: "user/edit_profile.html",
            resolve: {
                data : ['AuthService', function (AuthService) {
                    return AuthService.checkTokenStatus()
                }] 
            },
            controller: 'ProfileCtrl'
        })
        .when('/home',{
            templateUrl: 'index.html'
        })
        .when('/recipes/:id',{
            templateUrl: 'recipes/recipe.html',
            controller: 'RecipesCtrl'
        })
        .when('/recipes',{
            templateUrl: 'recipes/recipes.html',
            controller: 'RecipesCtrl'
        })
        .when('/beers',{
            templateUrl: 'beers/beers.html',
            resolve: {
                data : ['AuthService', function (AuthService) {
                    if (!AuthService.isLoggedIn()) {
                      return AuthService.checkTokenStatus()
                    }
                }] 
            },
            controller: 'BeersCtrl'
        })
         .when('/beers/new',{
            templateUrl: 'beers/new_beer.html',
            resolve: {
                data : ['AuthService', function (AuthService) {
                    return AuthService.checkTokenStatus()
                }]   
            },
            controller: 'BeerNewCtrl'
        })
        .when('/beers/:id',{
            templateUrl: 'beers/beer.html',
            resolve: {
                data : ['AuthService', function (AuthService) {
                    if (!AuthService.isLoggedIn()) {
                      return AuthService.checkTokenStatus()
                    }
                }]  
            },
            controller: 'BeersCtrl'
        })
        .when('/beers/:id/edit_beer',{
            templateUrl: 'edit/edit_beer.html',
            resolve: {
                data : ['AuthService', function (AuthService) {
                    return AuthService.checkTokenStatus()
                }]   
            },
            controller: 'BeerEditCtrl'
        })
        .when('/beers/:id/edit_recipe',{
            templateUrl: 'edit/edit_recipe.html',
            resolve: {
                data : ['AuthService', function (AuthService) {
                    return AuthService.checkTokenStatus()
                }] 
            },
            controller: 'BeerEditCtrl'
        })
        .when('/beers/:id/edit_stats',{
            templateUrl: 'edit/edit_stats.html',
            resolve: {
                data : ['AuthService', function (AuthService) {
                    return AuthService.checkTokenStatus()
                }] 
            },
            controller: 'BeerEditCtrl'
        })
        .otherwise({
            redirectTo: '/home'
        });
        
        
    
}]);

app.factory('AuthInterceptor', ['$location', '$rootScope', '$q', '$injector', function($location, $rootScope, $q, $injector) {
  var authInterceptor;
  authInterceptor = {
    request: function(config) {
      var token;
      token = void 0;
      if (localStorage.getItem('auth_token')) {
        token = angular.fromJson(localStorage.getItem('auth_token')).token;
      }
      if (token) {
        config.headers.Authorization = 'Bearer ' + token;
      }
      return config;
    },
    responseError: function(response) {
      if (response.status === 401 && $location.path() != '/home' && $location.path() != '/recipes') {
        localStorage.removeItem('auth_token');
        $rootScope.login_errorMsg = response.data.error;
        $location.path('/login');
        return $q.reject(response);
      }
      return response;
    }
  };
  return authInterceptor;
}])

// app.config(['$httpProvider', function($httpProvider) {
//   $httpProvider.interceptors.push('AuthInterceptor');
// }]);


app.config(['$httpProvider', function ($httpProvider){ 
    $httpProvider.defaults.xsrfCookieName = 'XSRF-TOKEN'
    $httpProvider.defaults.xsrfHeaderName = 'X-XSRF-TOKEN'
    $httpProvider.interceptors.push('AuthInterceptor');
}]);

// app.value('cgBusyDefaults',{
//   templateUrl: 'loading.html',
// });