class IngredientsController < ApplicationController
  def show
      id = params[:id]
      ingredients = Ingredient.find(id)
      render :json => {:message => {ingredients:ingredients}}
  end
end