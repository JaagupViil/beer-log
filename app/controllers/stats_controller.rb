class StatsController < ApplicationController

  
  before_action :authorize, only: [:update]
  
  def show
      id = params[:id]
      stats = Stat.find(id)
      beer = Beer.where(stat_id:stats.id)
      if(beer)
        authorize
        if(@current_user && @current_user.beers.where(id:beer.first.id))
          render :json => {:message => {stats:stats}}
        else
          msg = "Something went wrong, try again!"
          render :json => {:message => msg}
        end
        
      else
        render :json => {:message => {stats:stats}}
      end
        
  end
  
  def update
      stats = Stat.find((Beer.find params[:id]).stat_id)
      if(stats)
          stats.update_attributes!(params[:stats].permit(Stat.permitted_params))
          msg = "Stats succesfully updated"
      else
          msg = "Something went wrong, try again!"
      end
      render :json => {message: msg}
  end

  
end