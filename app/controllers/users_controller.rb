class UsersController < ApplicationController
    
    before_action :authorize
    
    def destroy
        User.destroy(@current_user.id)
        render :json => {message: "User deleted"}
    end
    
    def update
        begin
            @current_user.update(user_params)
            token = AuthToken.issue_token({ user_id: @current_user.id })
            msg = "User succesfully updated!"
            render :json => {email:@current_user.email, token: token, message: msg}
        rescue
            msg = "Something went wrong!"
            render :json => {message: msg}
        end
    
    end
    
    def ping
       puts "Got ping from: " + request.remote_ip
       render :json => {message: 'I am alive'}
    end

  private

    def user_params
      params.permit(:email, :password)
    end    
end