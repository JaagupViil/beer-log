class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  require 'auth_token'
  
  after_action :set_csrf_cookie_for_ng

  def set_csrf_cookie_for_ng
    cookies['XSRF-TOKEN'] = form_authenticity_token if protect_against_forgery?
  end
  
  def authorize
      # puts "Authorizing"
      # puts @current_user
      # token = request.headers['Authorization'].split(' ').last
      # payload, header = AuthToken.valid?(token)
      # puts payload,header
      begin
          token = request.headers['Authorization'].split(' ').last
          payload, header = AuthToken.valid?(token)
          puts payload,header
          @current_user = User.find_by(id: payload['user_id'])
          puts @current_user.inspect
      rescue
          render json: { error: 'You need to login or signup first' }, status: :unauthorized
      end
  end

  protected

  def verified_request?
    super || valid_authenticity_token?(session, request.headers['X-XSRF-TOKEN'])
  end
  
  
end
