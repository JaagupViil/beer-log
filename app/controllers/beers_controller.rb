class BeersController < ApplicationController
    
    before_action :authorize
    
    def show
        id = params[:id]
        if (id)
            begin
                beer = @current_user.beers.find(id)
                beer_stats = Stat.find(beer.stat_id)
                recipe = Recipe.find(beer.recipe_id)
                recipe_stats = Stat.find(recipe.stat_id)
                puts recipe.inspect
                ingredients = Ingredient.find(recipe.ingredient_id)
                puts recipe_stats.inspect
                render :json => {message: {beer: beer, beer_stats: beer_stats, recipe: recipe, 
                                recipe_stats:recipe_stats, ingredients: ingredients}}
            rescue
                render :json => {message: "This brew does not exist"}
            end
        else
            beers = @current_user.beers.order("brewed DESC")
            render :json => {message: {beers: beers}}
        end
    end
    
    def update
        begin
            beer = Beer.find params[:id]
            beer.update_attributes!(params[:beer].permit(Beer.permitted_params))
            msg = "Brew succesfully updated"
        rescue
            msg = "Something went wrong, try again!"
        end
        render :json => {message: msg}
    end
    
    def create
        begin
            beer_stats = Stat.create params[:beer_stats].permit(Stat.permitted_params)
            ingredients = Ingredient.create params[:ingredients].permit!
            
            beer = Beer.create params[:beer].permit(Beer.permitted_params)
            
            

            recipe = Recipe.create params[:recipe].permit(Recipe.permitted_params)
            if (recipe.rating == nil)
               recipe.rating = beer.rating 
            end
            recipe_stats = Stat.create params[:recipe_stats].permit(Stat.permitted_params)
            recipe.stat_id = recipe_stats.id
            recipe.ingredient_id = ingredients.id
            recipe.save!
            beer.recipe_id = recipe.id

            beer.stat_id = beer_stats.id
            beer.user_id = @current_user.id      
            beer.save!
            msg = "Brew was created succesfully!"
        rescue
            msg = "Something went wrong, try again!"
        end
        render :json => {message: msg, id:beer.id}
    end
    
    def destroy
        begin
            beer = Beer.find(params[:id])
            stats = Stat.find(beer.stat_id)
            recipe = Recipe.find(beer.recipe_id)
            beer.destroy!
            stats.destroy!
            recipe.destroy!
            msg = "Brew was succesfully deleted!"
        rescue
            msg = "Something went wrong, try again!"
        end
        render :json => {message: msg}   
    end
    
    def user_beer_statistics
        user_id = @current_user.id
        begin
            total_brews = User.find(user_id).beers.count
            user_recipe_ids = User.find(user_id).beers.pluck(:recipe_id)
            brew_styles = Recipe.where(id:user_recipe_ids).pluck(:style)
            styles_count = brew_styles.inject(Hash.new(0)) {|h,x| h[x]+=1;h}
            msg = {total:total_brews,styles_count:styles_count}
        rescue
            msg = "Something went wrong, try again!"
        end
        render :json => {message: msg}
    end
    
end