require 'net/http' 
require 'net/https'

class AuthController < ApplicationController
  require 'auth_token'

  layout false

  def verify_google_recaptcha(secret_key,captcha_response)
    url = URI.parse("https://www.google.com/recaptcha/api/siteverify?secret=#{secret_key}&response=#{captcha_response}")
    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    request = Net::HTTP::Post.new(url)
    response = http.request(request)
    JSON.parse(response.body)["success"] == true
  end

  def register
    secret = Rails.application.secrets.captcha_key
    correct_captcha = verify_google_recaptcha(secret, params[:captcha_response])
    @user = User.new(:email => params[:email], :password => params[:password])

    if (User.exists?(:email => @user.email))
      render json: { message: "Problem creating account: the e-mail already exists!" }, status: :forbidden
      return
    end

    if correct_captcha and @user.save
      render json: { message: "Account created succesfully!" }
    else
      render json: { message: "Problem creating account!" }, status: :forbidden
    end
  end
  
  #Log in process, issuing a token and authenticating user
  def authenticate
    @user = User.find_by(email: params[:email].downcase)
    if @user && @user.authenticate(params[:password])
      @token = AuthToken.issue_token({ user_id: @user.id })
    else
      render json: { error: "Invalid email/password combination" }, status: :unauthorized
    end
  end

  def token_status
    puts "Checking token status on the server side"
    token = params[:token]
    if AuthToken.valid?(token)
      head 200
    else
      render json: { error: 'You need to login or sign up!' }, status: :unauthorized
    end
  end

  private

    def user_params
      params.permit(:email, :password, :auth, :captcha_response)
    end
    
end