class RecipesController < ApplicationController
  
  before_action :authorize, only: [:update]
  
  def show
      id = params[:id]
      if (id)
        begin
          recipe = Recipe.find(id)
          ingredients = Ingredient.find(recipe.ingredient_id)
          stats = Stat.find(recipe.stat_id)
          render :json => {:message => {recipe:recipe,ingredients:ingredients,stats:stats}}
        rescue
          render :json => {:message => "This recipe does not exist!"}
        end
      else
        ids=Ingredient.where("array_length(fermentables,1)>=1").select(:id)
        recipes = Recipe.where(ingredient_id:ids)
        recipes.each do |r|
         # beer = Beer.where(:recipe_id => r.id).first
         # if (beer)
        #    user = User.find(beer.user_id)
            #puts user.email
         #   r.user =  user.email
            #puts user.inspect
            
          #end
          r.populateUserAndRating
          
          #puts email
        end
        render :json => {:message => {recipes:recipes}}
      end
      
  end
  
  def update
      puts params
      recipe = Recipe.find params[:recipe][:id]
      ingredients = Ingredient.find(recipe.ingredient_id)
      recipe_stats = Stat.find(recipe.stat_id)
      
      msg = "Something went wrong, try again!"
      if(recipe)
          begin
            ingredients.update_attributes!(params[:ingredients].permit!)
            recipe.update_attributes!(params[:recipe].permit(Recipe.permitted_params))
            recipe_stats.update_attributes!(params[:recipe_stats].permit(Stat.permitted_params))
            msg = "Recipe succesfully updated"
          rescue
          end
      end
      puts msg
      render :json => {message: msg}
  end
  
end