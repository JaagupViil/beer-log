class AddRecipeToBeers < ActiveRecord::Migration
  def change
    add_reference :beers, :recipe, index: true, foreign_key: true
  end
end
