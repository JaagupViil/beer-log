class AddStatToBeers < ActiveRecord::Migration
  def change
    add_reference :beers, :stat, index: true, foreign_key: true
  end
end
