class CreateIngredients < ActiveRecord::Migration
  def change
    create_table :ingredients do |t|
      t.string :hops, :array => true, :default => []
      t.string :fermentables, :array => true, :default => []
      t.string :yeast, :array => true, :default => []
      t.string :other, :array => true, :default => []

      t.timestamps null: false
    end
  end
end
