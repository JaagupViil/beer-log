class AddStatToRecipes < ActiveRecord::Migration
  def change
    add_reference :recipes, :stat, index: true, foreign_key: true
  end
end
