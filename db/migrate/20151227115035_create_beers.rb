class CreateBeers < ActiveRecord::Migration
  def change
    create_table :beers do |t|
      t.string :name
      t.date :brewed
      t.date :bottled
      t.integer :rating
      t.string :notes

      t.timestamps null: false
    end
  end
end
