class CreateStats < ActiveRecord::Migration
  def change
    create_table :stats do |t|
      t.decimal :OG
      t.decimal :FG
      t.integer :IBU
      t.integer :SRM

      t.timestamps null: false
    end
  end
end
