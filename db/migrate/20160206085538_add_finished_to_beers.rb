class AddFinishedToBeers < ActiveRecord::Migration
  def change
    add_column :beers, :finished, :date
  end
end
