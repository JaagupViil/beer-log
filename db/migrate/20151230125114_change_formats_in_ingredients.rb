class ChangeFormatsInIngredients < ActiveRecord::Migration
  def change
    remove_column :ingredients, :hops
    remove_column :ingredients, :fermentables
    remove_column :ingredients, :yeast
    remove_column :ingredients, :other
    add_column :ingredients, :hops, :json, array:true, default: []
    add_column :ingredients, :fermentables, :json, array:true, default: []
    add_column :ingredients, :yeast, :json, array:true, default: []
    add_column :ingredients, :other, :json, array:true, default: []
  end
end
