class CreateRecipes < ActiveRecord::Migration
  def change
    create_table :recipes do |t|
      t.string :name
      t.string :style
      t.string :batch_size
      t.integer :rating
      t.string :notes

      t.timestamps null: false
    end
  end
end
