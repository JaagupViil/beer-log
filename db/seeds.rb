# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# @ingredients = Ingredient.create(hops:[{'name'=>'Saaz','amount'=>'30g'},
# {'name'=>'Perle','amount'=>'25g'}],fermentables:[{'name'=>'Munton Irish kit','amount'=>'1,5kg'},{'name'=>'LME','amount'=>'1kg'}],yeast:[{'name'=>"S0-4"}])
# @john = User.create(name:"Johnson",email:"a@a.com",password:"pass")
# @stout_stats = Stat.create(OG: 1.045, FG:1.023, IBU: 20, SRM: 17)
# @recipe = Recipe.create(name:"Guiness clone",style: "Stout",batch_size:"20L",rating:5, stat_id:@stout_stats.id,ingredient_id:@ingredients.id)
# @recipe2 = Recipe.create(name:"Lager clone",style: "Lager",batch_size:"20L",rating:5, stat_id:@stout_stats.id,ingredient_id:@ingredients.id)


# @beer_stats = Stat.create(OG: 1.045, FG:1.020, IBU: 20, SRM: 17)
# @beer_stats2 = Stat.create(OG: 1.022, FG:1.020, IBU: 20, SRM: 5)
# @beer = Beer.create(name:"Minu guinessi kloon",brewed:"2015-12-27",bottled:"2016-01-27",recipe_id:@recipe.id,rating:5,stat_id:@beer_stats.id,user_id:@john.id)
# @beer2 = Beer.create(name:"Minu lageri kloon",brewed:"2015-12-27",bottled:"2016-01-27",recipe_id:@recipe2.id,rating:2,stat_id:@beer_stats2.id,user_id:@john.id)

# (1..10).each do |n|
#   Recipe.create(name:"Recipe #{n}",style: "Style #{n}", rating: n,ingredient_id:@ingredients.id)
# end

# names = ['a','b','c','d','e','f','g','h','i','j','k','l']
# (0..11).each do |n|
#     Beer.create(name:names[n],user_id:@john.id,rating:n,brewed:"2015-#{n}-10",bottled:"2015-3-#{n*2}")
# end

# Recipe.create(name:"<script> alert('Hello, World!') </script>",style: "Style injection", rating: 3,ingredient_id:@ingredients.id)