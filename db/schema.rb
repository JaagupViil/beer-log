# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190316151616) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "beers", force: :cascade do |t|
    t.string   "name"
    t.date     "brewed"
    t.date     "bottled"
    t.integer  "rating"
    t.string   "notes"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "recipe_id"
    t.integer  "stat_id"
    t.integer  "user_id"
    t.date     "finished"
  end

  add_index "beers", ["recipe_id"], name: "index_beers_on_recipe_id", using: :btree
  add_index "beers", ["stat_id"], name: "index_beers_on_stat_id", using: :btree
  add_index "beers", ["user_id"], name: "index_beers_on_user_id", using: :btree

  create_table "ingredients", force: :cascade do |t|
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.json     "hops",         default: [],              array: true
    t.json     "fermentables", default: [],              array: true
    t.json     "yeast",        default: [],              array: true
    t.json     "other",        default: [],              array: true
  end

  create_table "recipes", force: :cascade do |t|
    t.string   "name"
    t.string   "style"
    t.string   "batch_size"
    t.string   "notes"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "stat_id"
    t.integer  "ingredient_id"
  end

  add_index "recipes", ["ingredient_id"], name: "index_recipes_on_ingredient_id", using: :btree
  add_index "recipes", ["stat_id"], name: "index_recipes_on_stat_id", using: :btree

  create_table "stats", force: :cascade do |t|
    t.decimal  "OG"
    t.decimal  "FG"
    t.integer  "IBU"
    t.integer  "SRM"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "password_digest"
    t.string   "email"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_foreign_key "beers", "recipes"
  add_foreign_key "beers", "stats"
  add_foreign_key "beers", "users"
  add_foreign_key "recipes", "ingredients"
  add_foreign_key "recipes", "stats"
end
