Rails.application.routes.draw do
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'home#index'
  
  get '/recipes', to: 'recipes#show', defaults: {format: :json}
  post '/recipes/update', to: 'recipes#update', defaults: {format: :json}
  
  get '/stats', to: 'stats#show', defaults: {format: :json}
  post '/stats/update', to: 'stats#update', defaults: {format: :json}
  
  get '/ingredients', to: 'ingredients#show', defaults: {format: :json}
  
  get '/beers', to: 'beers#show'
  get '/beers/delete', to: 'beers#destroy'
  get '/beers/statistics', to: 'beers#user_beer_statistics'
  post '/beers/update', to: 'beers#update'
  post '/beers/create', to: 'beers#create'
  
  post '/user/update', to: 'users#update'
  get '/user/delete', to: 'users#destroy'
  get '/staying/alive/ping', to: 'users#ping', defaults: {format: :json}
  
  post '/auth/register', to: 'auth#register', defaults: {format: :json}
  post '/auth/authenticate', to: 'auth#authenticate', defaults: {format: :json}
  get '/auth/token_status', to: 'auth#token_status', defaults: {format: :json}
  
  
  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
